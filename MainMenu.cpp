#include "MainMenu.h"
#include "IwGx.h"
#include "input.h"
#include "resources.h"
#include "game.h"

 MainMenu::MainMenu()
 {
 }
    
MainMenu::~MainMenu()
{
}

void MainMenu::Update(float deltaTime, float alphaMul)
{
    if (!m_IsActive)
        return;

    Scene::Update(deltaTime, alphaMul);

    // Detect screen tap
    if (m_IsInputActive && m_Manager->GetCurrent() == this && !g_pInput->m_Touched && g_pInput->m_PrevTouched)
    {
        g_pInput->Reset();
        
		// Switch to game scene
        Game* game = (Game*)g_pSceneManager->Find("game");
        g_pSceneManager->SwitchTo(game);
    }
}

void MainMenu::Render()
{
    Scene::Render();
}

void MainMenu::Init()
{
    Scene::Init();

    Game* game = (Game*)g_pSceneManager->Find("game");

    // Create menu background
	// Create menu background
    CSprite* background = new CSprite();
    background->m_X = (float)IwGxGetScreenWidth() / 2;
    background->m_Y = (float)IwGxGetScreenHeight() / 2;
    background->SetImage(g_pResources->getMenuBG());
    background->m_W = background->GetImage()->GetWidth();
    background->m_H = background->GetImage()->GetHeight();
    background->m_AnchorX = 0.5;
    background->m_AnchorY = 0.5;
    // Fit background to screen size
    background->m_ScaleX = (float)IwGxGetScreenWidth() / background->GetImage()->GetWidth();
    background->m_ScaleY = (float)IwGxGetScreenHeight() / background->GetImage()->GetHeight();
    AddChild(background);

    // Create Start Game button

    // Create Start Game button text

}
