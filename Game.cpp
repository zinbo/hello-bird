#include "Game.h"
#include "input.h"
#include "resources.h"
#include "IwGx.h"
#include "IwHashString.h"
#include <math.h>
#include <unistd.h>

bool Game::frontPipeReset = false;
bool Game::backPipeReset = false;
Game::GameStatus Game::gameStatus = NOT_STARTED;

Game::Game()
	:_DELAY(0.5f), _JUMP_HEIGHT(100), _JUMP_TIME(0.5f), _DISTANCE_BETWEEN_PIPES(200), _PIPE_SPEED(3.2f), _INITIAL_BACK_PIPE_SPEED(4.8f), _ACCELERATION(400)
{
	
}

void Game::ResetScore()
{
	//Reset score.
	currentRoundScore = 0;
	char str[16];
    snprintf(str, 16, "%d", currentRoundScore);
    scoreLabel->m_Text = str;
}

void Game::ResetGame(Timer* timer, void* userData)
{
	//Set the bird and pipes back to their origin position
	Game* self = (Game*)userData;
	self->birdSprite->m_X = (float)IwGxGetScreenWidth() / 2;
    self->birdSprite->m_Y = (float)IwGxGetScreenHeight() / 2;
	self->gameStatus = NOT_STARTED;
	self->InitPipePositions();
	
	//Reset score.
	self->ResetScore();


}

void Game::CheckForBirdOutOfBounds()
{
	//if the bird has gone too high, or too low, then reset the game
	if(birdSprite->m_Y >= (float)IwGxGetScreenHeight()-2 || birdSprite->m_Y <= 0)
	{
		m_Timers.Add(new Timer(_DELAY, 1, &Game::ResetGame, (void*)this));
		FreezeGame();
	}
}

void Game::StartPipesMoving()
{
	possibleFrontPoint = true;
	possibleBackPoint = true;

	pipe_Tweener.Tween(_PIPE_SPEED,
		FLOAT, &frontTopPipeSprite->m_X, (float) -frontTopPipeSprite->GetImage()->GetWidth(),
		FLOAT, &frontTopPipeSprite->m_Y, (float) frontTopPipeSprite->m_Y,
		FLOAT, &frontBottomPipeSprite->m_X, (float) -frontBottomPipeSprite->GetImage()->GetWidth(),
		FLOAT, &frontBottomPipeSprite->m_Y, (float) frontBottomPipeSprite->m_Y,
		ONCOMPLETE, &Game::ResetFrontPipes,
		END);

	//The back pipe has a faster initial speed as it starts further away
	pipe_Tweener.Tween(_INITIAL_BACK_PIPE_SPEED,
		FLOAT, &backTopPipeSprite->m_X, (float) -backTopPipeSprite->GetImage()->GetWidth(),
		FLOAT, &backTopPipeSprite->m_Y, (float) backTopPipeSprite->m_Y,
		FLOAT, &backBottomPipeSprite->m_X, (float) -backBottomPipeSprite->GetImage()->GetWidth(),
		FLOAT, &backBottomPipeSprite->m_Y, (float) backBottomPipeSprite->m_Y,
		ONCOMPLETE, &Game::ResetBackPipes,
		END);

}

//Check for a collision with the front toppipe
bool Game::FrontTopPipeCollision(float & left, float & top)
{

	return !((left > frontTopPipeSprite->m_X + frontTopPipeSprite->GetImage()->GetWidth()*graphicsScale) ||
		(top > frontTopPipeSprite->m_Y + frontTopPipeSprite->GetImage()->GetHeight()*graphicsScale) ||
		(frontTopPipeSprite->m_X > left + birdSprite->GetImage()->GetWidth()*graphicsScale) ||
		(frontTopPipeSprite->m_Y > top + birdSprite->GetImage()->GetHeight()*graphicsScale));
}

//Check for a collision with the front bottom pipe
bool Game::FrontBottomPipeCollision(float & left, float & top)
{
	return !((left > frontBottomPipeSprite->m_X + frontBottomPipeSprite->GetImage()->GetWidth()*graphicsScale) ||
		(top > frontBottomPipeSprite->m_Y + frontBottomPipeSprite->GetImage()->GetHeight()*graphicsScale) ||
		(frontBottomPipeSprite->m_X > left + birdSprite->GetImage()->GetWidth()*graphicsScale) ||
		(frontBottomPipeSprite->m_Y > top + birdSprite->GetImage()->GetHeight()*graphicsScale));
}

//Check for a collision with the back top pipe
bool Game::BackTopPipeCollision(float & left, float & top)
{

	return !((left > backTopPipeSprite->m_X + backTopPipeSprite->GetImage()->GetWidth()*graphicsScale) ||
		(top > backTopPipeSprite->m_Y + backTopPipeSprite->GetImage()->GetHeight()*graphicsScale) ||
		(backTopPipeSprite->m_X > left + birdSprite->GetImage()->GetWidth()*graphicsScale) ||
		(backTopPipeSprite->m_Y > top + birdSprite->GetImage()->GetHeight()*graphicsScale));
}

//Check for a collision with the back bottom pipe
bool Game::BackBottomPipeCollision(float & left, float & top)
{
	return !((left > backBottomPipeSprite->m_X + backBottomPipeSprite->GetImage()->GetWidth()*graphicsScale) ||
		(top > backBottomPipeSprite->m_Y + backBottomPipeSprite->GetImage()->GetHeight()*graphicsScale) ||
		(backBottomPipeSprite->m_X > left + birdSprite->GetImage()->GetWidth()*graphicsScale) ||
		(backBottomPipeSprite->m_Y > top + birdSprite->GetImage()->GetHeight()*graphicsScale));
}


void Game::CheckForPipeCollision()
{
	float left = birdSprite->m_X - birdSprite->GetImage()->GetWidth()/2;
	float top = birdSprite->m_Y - birdSprite->GetImage()->GetHeight()/2;

	//Check if the bird has collided with a pipe, if it has then reset the game.
	if(FrontTopPipeCollision(left, top) || FrontBottomPipeCollision(left, top) || BackTopPipeCollision(left, top) || BackBottomPipeCollision(left, top) )
	{
		m_Timers.Add(new Timer(_DELAY, 1, &Game::ResetGame, (void*)this));
		FreezeGame();
	}

}

void Game::FreezeGame()
{
	//Stop all animations and set the game status to waiting for respawn
	gameStatus = WAITING;
	m_Tweener.Clear();
	pipe_Tweener.Clear();
}

void Game::ResetFrontPipes()
{
	Game::frontPipeReset = true;
}

void Game::ResetBackPipes()
{
	Game::backPipeReset = true;
}

void Game::ResetFrontPipePositions()
{
	//Randomly generate a height and set the pipe back to the right of the screen.
	int heightOffset = rand() % ((int)IwGxGetScreenHeight() - (int)(_DISTANCE_BETWEEN_PIPES*graphicsScale));

	frontBottomPipeSprite->m_X = (float)IwGxGetScreenWidth();
	frontBottomPipeSprite->m_Y = heightOffset + _DISTANCE_BETWEEN_PIPES*graphicsScale;

	frontTopPipeSprite->m_X = (float)IwGxGetScreenWidth();
	frontTopPipeSprite->m_Y = -(float)IwGxGetScreenHeight() + heightOffset;
}

void Game::ResetBackPipePositions()
{
	//Randomly generate a height and set the pipe back to the right of the screen.
	int heightOffset = rand() % ((int)IwGxGetScreenHeight() - (int)(_DISTANCE_BETWEEN_PIPES*graphicsScale));

	backBottomPipeSprite->m_X = (float)IwGxGetScreenWidth();
	backBottomPipeSprite->m_Y = heightOffset + _DISTANCE_BETWEEN_PIPES*graphicsScale;

	backTopPipeSprite->m_X = (float)IwGxGetScreenWidth();
	backTopPipeSprite->m_Y = -(float)IwGxGetScreenHeight() + heightOffset;
}

void Game::MakeBirdJumpIfTouched()
{
	//if the player has touched the screen then make the bird jump.
	if (m_IsInputActive && m_Manager->GetCurrent() == this && !g_pInput->m_Touched && g_pInput->m_PrevTouched)
		{
			g_pInput->Reset();
     
			//If the game hasn't yet started then start the pipes moving.
			if(gameStatus == NOT_STARTED)
			{
				gameStatus = GAME_STARTED;
				StartPipesMoving();
			}
			//Clear any other jumping animation currently happening
			m_Tweener.Clear();

			// Make the bird jump
			m_Tweener.Tween(_JUMP_TIME,
				FLOAT, &birdSprite->m_X, (float) birdSprite->m_X,
				FLOAT, &birdSprite->m_Y, (float) birdSprite->m_Y-(_JUMP_HEIGHT*graphicsScale),
				EASING, Ease::sineOut, 
				END);
		}
}

void Game::MakeBirdFallIfNotJumping()
{
	//If the game has started, and the bird isn't current jumping, then the bird needs to start falling.
	if((m_Tweener.GetNumTweens() == 0) && (gameStatus == GAME_STARTED))
		{
			float time = ((float)IwGxGetScreenHeight() - birdSprite->m_Y)/(_ACCELERATION*graphicsScale);
			m_Tweener.Tween(time,
				FLOAT, &birdSprite->m_X, (float) birdSprite->m_X,
				FLOAT, &birdSprite->m_Y, (float)IwGxGetScreenHeight(),
				EASING, Ease::sineIn,
				END);
		}
}

void Game::Update(float deltaTime, float alphaMul)
{
	//If this isn't the active scene then do nothing
    if (!m_IsActive)
        return;

    // Update base scene
    Scene::Update(deltaTime, alphaMul);
	pipe_Tweener.Update(deltaTime);
	
	//If the player hasn't died and waiting for respawn, then play the game!
	if(gameStatus != WAITING)
	{
		// Detect screen tap and make bird jump
		MakeBirdJumpIfTouched();

		//If the bird has stopped jumping then make it fall.
		MakeBirdFallIfNotJumping();

		//Check if the bird has collided with the screen bounds or the pipes
		if(gameStatus == GAME_STARTED)
		{
			CheckForBirdOutOfBounds();
			CheckForPipeCollision();
		}
		
	}
	//If we're waiting for respawn _DELAY then throw away any input
	else
	{
		g_pInput->Reset();
	}
	
	//Check if we need to reset the position of the front pipes and make them move to the edge of the screen
	if(Game::gameStatus == GAME_STARTED && Game::frontPipeReset)
	{
		Game::frontPipeReset = false;
		ResetFrontPipePositions();
		//This is used so that we know we can claim a point if the pipe passes the middle of the screen
		possibleFrontPoint = true;

		pipe_Tweener.Tween(_PIPE_SPEED,
			FLOAT, &frontTopPipeSprite->m_X, (float) -frontTopPipeSprite->GetImage()->GetWidth()*graphicsScale,
			FLOAT, &frontTopPipeSprite->m_Y, (float) frontTopPipeSprite->m_Y,
			FLOAT, &frontBottomPipeSprite->m_X, (float) -frontBottomPipeSprite->GetImage()->GetWidth()*graphicsScale,
			FLOAT, &frontBottomPipeSprite->m_Y, (float) frontBottomPipeSprite->m_Y,
			ONCOMPLETE, &Game::ResetFrontPipes,
			END);
	}

	//Check if we need to reset the position of the back pipes and make them move to the edge of the screen
	if(Game::gameStatus == GAME_STARTED && Game::backPipeReset)
	{
		Game::backPipeReset = false;
		ResetBackPipePositions();
		//This is used so that we know we can claim a point if the pipe passes the middle of the screen
		possibleBackPoint = true;

		pipe_Tweener.Tween(_PIPE_SPEED,
			FLOAT, &backTopPipeSprite->m_X, (float) -backTopPipeSprite->GetImage()->GetWidth()*graphicsScale,
			FLOAT, &backTopPipeSprite->m_Y, (float) backTopPipeSprite->m_Y,
			FLOAT, &backBottomPipeSprite->m_X, (float) -backBottomPipeSprite->GetImage()->GetWidth()*graphicsScale,
			FLOAT, &backBottomPipeSprite->m_Y, (float) backBottomPipeSprite->m_Y,
			ONCOMPLETE, &Game::ResetBackPipes,
			END);
	}
	//If we haven't already added a point for the player passing the front pipe, then add a point, and make sure that the player can't claim another point from this pipe for this animation
	if(possibleFrontPoint && frontTopPipeSprite->m_X < ((float)IwGxGetScreenWidth()/2 - (birdSprite->GetImage()->GetWidth() + frontTopPipeSprite->GetImage()->GetWidth())))
	{
		possibleFrontPoint = false;
		incrementScore();
	}
	//If we haven't already added a point for the player passing the back pipe, then add a point, and make sure that the player can't claim another point from this pipe for this animation
	else if(possibleBackPoint && backTopPipeSprite->m_X < ((float)IwGxGetScreenWidth()/2 - (birdSprite->GetImage()->GetWidth() + backTopPipeSprite->GetImage()->GetWidth())))
	{
		possibleBackPoint = false;
		incrementScore();
	}
}	

void Game::Render()
{
    Scene::Render();
}

void Game::initUI()
{
	//Need to create background image
	CSprite* background = new CSprite();
    background->m_X = (float)IwGxGetScreenWidth() / 2;
    background->m_Y = (float)IwGxGetScreenHeight() / 2;
    background->SetImage(g_pResources->getGameBG());
    background->m_W = background->GetImage()->GetWidth();
    background->m_H = background->GetImage()->GetHeight();
    background->m_AnchorX = 0.5;
    background->m_AnchorY = 0.5;
    // Fit background to screen size
    background->m_ScaleX = (float)IwGxGetScreenWidth() / background->GetImage()->GetWidth();
    background->m_ScaleY = (float)IwGxGetScreenHeight() / background->GetImage()->GetHeight();
    AddChild(background);

	 // Create score label text
    CLabel* scoreLabelText = new CLabel();
    scoreLabelText->m_X = 10;
    scoreLabelText->m_Y = 0;
    scoreLabelText->m_W = (float)IwGxGetScreenWidth();
    scoreLabelText->m_H = 30;
    scoreLabelText->m_Text = "Score:";
    scoreLabelText->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    scoreLabelText->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    scoreLabelText->m_Font = g_pResources->getFont();
    scoreLabelText->m_Color = CColor(0xff, 0xff, 0x30, 0xff);
    AddChild(scoreLabelText);

    // Create score label (displays actual score)
    scoreLabel = new CLabel();
    scoreLabel->m_X = 80;
    scoreLabel->m_Y = 0;
    scoreLabel->m_W = (float)IwGxGetScreenWidth();
    scoreLabel->m_H = 30;
    scoreLabel->m_Text = "0";
    scoreLabel->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    scoreLabel->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    scoreLabel->m_Font = g_pResources->getFont();
    scoreLabelText->m_Color = CColor(0xff, 0xff, 0xff, 0xff);
    AddChild(scoreLabel);
	
}

void Game::Init()
{
	//Set seed using time
	srand (time(NULL));
	Scene::Init();

	//Set the graphics scale
	graphicsScale = (float)IwGxGetScreenWidth() / GRAPHIC_DESIGN_WIDTH;


	//Set score to 0
    currentRoundScore = 0;

    // Initialise UI
    initUI();

	//Create bird and place it in the scene
    birdSprite = new CSprite();
    birdSprite->SetImage(g_pResources->getBird());
    birdSprite->m_X = (float)IwGxGetScreenWidth() / 2;
    birdSprite->m_Y = (float)IwGxGetScreenHeight() / 2;
    birdSprite->m_W = birdSprite->GetImage()->GetWidth();
    birdSprite->m_H = birdSprite->GetImage()->GetHeight();
    birdSprite->m_AnchorX = 0.5;
	birdSprite->m_AnchorY = 0.5;
	birdSprite->m_ScaleX = graphicsScale;
	birdSprite->m_ScaleY = graphicsScale;
    AddChild(birdSprite);

	//Initialise pipes and add them to the scene
	initPipes();
}

void Game::incrementScore()
{
    // Add to score
    currentRoundScore++;

    // Update score label text to show new score
    char str[16];
    snprintf(str, 16, "%d", currentRoundScore);
    scoreLabel->m_Text = str;
}

void Game::InitPipePositions()
{
	//Randomly generate 2 height offsets so that the pipes appear at random heights
	int heightOffset1 = rand() % ((int)IwGxGetScreenHeight() - (int)(_DISTANCE_BETWEEN_PIPES*graphicsScale));
	int heightOffset2 = rand() % ((int)IwGxGetScreenHeight() - (int)(_DISTANCE_BETWEEN_PIPES*graphicsScale));

	//Place the pipe so that it's edge starts at the height offsets
	frontTopPipeSprite->m_X = (float)IwGxGetScreenWidth();
	frontTopPipeSprite->m_Y = -(float)IwGxGetScreenHeight() + heightOffset1;

	//Place the bottom pipe so that it is _DISTANCE_BETWEEN_PIPES away from the top pipe
	frontBottomPipeSprite->m_X = (float)IwGxGetScreenWidth();
	frontBottomPipeSprite->m_Y = heightOffset1 + _DISTANCE_BETWEEN_PIPES*graphicsScale;

	//Repeat for the bottom pipes.
	backTopPipeSprite->m_X = (float)IwGxGetScreenWidth() + ((float)IwGxGetScreenWidth() / 2);
	backTopPipeSprite->m_Y = -(float)IwGxGetScreenHeight() + heightOffset2;

	backBottomPipeSprite->m_X = (float)IwGxGetScreenWidth() + ((float)IwGxGetScreenWidth() / 2);
	backBottomPipeSprite->m_Y = heightOffset2 + _DISTANCE_BETWEEN_PIPES*graphicsScale;
}

void Game::initPipes()
{
	//front top pipe initialisation
	frontTopPipeSprite = new CSprite();
	frontTopPipeSprite->SetImage(g_pResources->getTopPipe());
	frontTopPipeSprite->m_W = frontTopPipeSprite->GetImage()->GetWidth();
	frontTopPipeSprite->m_H = frontTopPipeSprite->GetImage()->GetHeight();
	frontTopPipeSprite->m_ScaleX = graphicsScale;
	frontTopPipeSprite->m_ScaleY = graphicsScale;
	

	//front bottom pipe initialisation
	frontBottomPipeSprite = new CSprite();
	frontBottomPipeSprite->SetImage(g_pResources->getBottomPipe());
	frontBottomPipeSprite->m_W = frontBottomPipeSprite->GetImage()->GetWidth();
	frontBottomPipeSprite->m_H = frontBottomPipeSprite->GetImage()->GetHeight();
	frontBottomPipeSprite->m_ScaleX = graphicsScale;
	frontBottomPipeSprite->m_ScaleY = graphicsScale;
	
	//back top pipe initialisation
	backTopPipeSprite = new CSprite();
	backTopPipeSprite->SetImage(g_pResources->getTopPipe2());
	backTopPipeSprite->m_W = backTopPipeSprite->GetImage()->GetWidth();
	backTopPipeSprite->m_H = backTopPipeSprite->GetImage()->GetHeight();
	backTopPipeSprite->m_ScaleX = graphicsScale;
	backTopPipeSprite->m_ScaleY = graphicsScale;
	

	//back bottom pipe initialisation
	backBottomPipeSprite = new CSprite();
	backBottomPipeSprite->SetImage(g_pResources->getBottomPipe2());
	backBottomPipeSprite->m_W = backBottomPipeSprite->GetImage()->GetWidth();
	backBottomPipeSprite->m_H = backBottomPipeSprite->GetImage()->GetHeight();
	backBottomPipeSprite->m_ScaleX = graphicsScale;
	backBottomPipeSprite->m_ScaleY = graphicsScale;

	//Initialise pipe positions
	InitPipePositions();

	//Add all the pipes as children to the scene.
	AddChild(frontBottomPipeSprite);
	AddChild(frontTopPipeSprite);
	AddChild(backBottomPipeSprite);
	AddChild(backTopPipeSprite);
}
