#pragma once
#include "scene.h"
#include "sceneManager.h"

#define GRAPHIC_DESIGN_WIDTH    320

class Game : public Scene
{
protected:
    // Sprites
    CSprite*            birdSprite;
	CSprite*            frontTopPipeSprite;
	CSprite*            frontBottomPipeSprite;
	CSprite*            backTopPipeSprite;
	CSprite*            backBottomPipeSprite;

	// Tween manager for the pipes
	CTweenManager       pipe_Tweener;

	//Label for the score
	CLabel*				scoreLabel;

	//Score
	

public:
	Game();
	~Game() {};
	
	//Status of the game
	enum GameStatus		{NOT_STARTED, GAME_STARTED, WAITING};
	static GameStatus	gameStatus;

	//Used to control when to reset the pipes to the right of the screen
	static bool			frontPipeReset;
	static bool			backPipeReset;

	//Used to render game
    void            Init();
    void            Update(float deltaTime = 0.0f, float alphaMul = 1.0f);
    void            Render();
    void            switchToScene(const char* scene_name);

private:
	//Methods and variables for rendering
    void            initUI();
	float			graphicsScale;

	//Methods and variables for pipes
	void			initPipes();
	void			StartPipesMoving();
	static void		ResetFrontPipes();
	static void		ResetBackPipes();
	void			ResetFrontPipePositions();
	void			ResetBackPipePositions();
	void			InitPipePositions();
	bool			FrontTopPipeCollision(float & left, float & top);
	bool			FrontBottomPipeCollision(float & left, float & top);
	bool			BackTopPipeCollision(float & left, float & top);
	bool			BackBottomPipeCollision(float & left, float & top);
	void			CheckForPipeCollision();

	//Methods and variables for Bird
	void			CheckForBirdOutOfBounds();
	void			MakeBirdJumpIfTouched();
	void			MakeBirdFallIfNotJumping();

	//Methods and variables for resetting the game when there is a collision
	static void		ResetGame(Timer* timer, void* userData);
	void			FreezeGame();
	
	//Methods and variables for score
	void            incrementScore();
	void			ResetScore();
	bool			possibleFrontPoint;
	bool			possibleBackPoint;
	int             currentRoundScore;

	//consts
	
	const float _DELAY;
	const int _JUMP_HEIGHT;
	const float _JUMP_TIME;
	const int _DISTANCE_BETWEEN_PIPES;
	const float _PIPE_SPEED;
	const float _INITIAL_BACK_PIPE_SPEED;
	const int _ACCELERATION;
	
};

