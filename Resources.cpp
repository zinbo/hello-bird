#include "Resources.h"


Resources::Resources()
{
	MenuBG = Iw2DCreateImage("textures/mainMenu.png");
    GameBG = Iw2DCreateImage("textures/Game.png");
	bird = Iw2DCreateImage("textures/bird.png");
	topPipe = Iw2DCreateImage("textures/pipe down.png");
	bottomPipe = Iw2DCreateImage("textures/pipe up.png");
	topPipe2 = Iw2DCreateImage("textures/pipe down2.png");
	bottomPipe2 = Iw2DCreateImage("textures/pipe up2.png");

	// Load fonts
    Font = Iw2DCreateFont("fonts/arial8.gxfont");
}


Resources::~Resources(void)
{
	delete MenuBG;
    delete GameBG;
	delete bird;
	delete topPipe;
	delete bottomPipe;
	delete topPipe2;
	delete bottomPipe2;
	delete Font;
}

// Global resources
Resources* g_pResources = 0;