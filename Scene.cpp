#include "Scene.h"
#include "IwGx.h"

Scene::Scene() : m_NameHash(0), m_IsActive(true), m_IsInputActive(false)
{
    m_IsVisible = false;
}

Scene::~Scene()
{
}

void Scene::SetName(const char* name)
{
    m_NameHash = IwHashString(name);
}

void Scene::Init()
{
}

void Scene::Update(float deltaTime, float alphaMul)
{
    if (!m_IsActive)
        return;

	m_Tweener.Update(deltaTime);
	m_Timers.Update(deltaTime);

    CNode::Update(deltaTime, alphaMul);
}

void Scene::Render()
{
    CNode::Render();
}
