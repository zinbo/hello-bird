#pragma once

#include "Iw2D.h"
#include "Iw2DSceneGraph.h"
#include <memory>

class Resources
{
protected:
    CIw2DImage*     MenuBG;
    CIw2DImage*     GameBG;
	CIw2DImage*     bird;
	CIw2DImage*		topPipe;
	CIw2DImage*		bottomPipe;
	CIw2DImage*		topPipe2;
	CIw2DImage*		bottomPipe2;
	CIw2DFont*      Font;

public:
    CIw2DImage*     getMenuBG()                 { return MenuBG; }
    CIw2DImage*     getGameBG()                 { return GameBG; }
	CIw2DImage*     getBird()                 { return bird; }
	CIw2DImage*     getTopPipe()                 { return topPipe; }
	CIw2DImage*     getBottomPipe()                 { return bottomPipe; }
	CIw2DImage*     getTopPipe2()                 { return topPipe2; }
	CIw2DImage*     getBottomPipe2()                 { return bottomPipe2; }
	CIw2DFont*      getFont()                   { return Font; }

public:
    Resources();
    ~Resources();
};

extern Resources* g_pResources;

