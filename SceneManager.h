#pragma once
#include <list>
class Scene;

class SceneManager
{
protected:
    Scene*              m_Current;              // Currently active scene
    Scene*              m_Next;                 // Next scene (scene that is being switched to)
    std::list<Scene*>   m_Scenes;               // Scenes list
public:
    Scene*  GetCurrent()                { return m_Current; }

public:
    SceneManager();
    ~SceneManager();

    void    SwitchTo(Scene* scene);
    void    Update(float deltaTime = 0.0f);
    void    Render();
    void    Add(Scene* scene);
    void    Remove(Scene* scene);
    Scene*  Find(const char* name);

};

extern SceneManager* g_pSceneManager;

